// Constants in milliseconds
const ONE_DAY = 24 * 60 * 60 * 1000
const ONE_HOUR = 60 * 60 * 1000
const ONE_MINUTE = 60 * 1000

// today date
let tempDate = new Date()
let tempYear = tempDate.getFullYear()
let tempMonth = tempDate.getMonth()
let tempDay = tempDate.getDate()

// 10 days from now
const futureDate = new Date(tempYear, tempMonth, tempDay + 11, 11, 30, 0)

// future time in ms
const futureTime = futureDate.getTime()

export const getRemainingTime = () => {
  const today = new Date().getTime()

  // remaining time in ms
  const t = futureTime - today

  // calculate all values
  let days = Math.floor(t / ONE_DAY)
  let hours = Math.floor((t % ONE_DAY) / ONE_HOUR)
  let minutes = Math.floor((t % ONE_HOUR) / ONE_MINUTE)
  let seconds = Math.floor((t % ONE_MINUTE) / 1000)

  // set values array
  const values = [days, hours, minutes, seconds]

  return values
}
