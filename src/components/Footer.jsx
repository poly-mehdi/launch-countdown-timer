import iconFacebook from '../images/icon-facebook.svg'
import iconInstagram from '../images/icon-instagram.svg'
import iconPinterest from '../images/icon-pinterest.svg'

const Footer = () => {
  return (
    <footer>
      <div className="link-container">
        <a href="#">
          <img src={iconFacebook} alt="facebook-logo" />
        </a>
        <a href="#">
          <img src={iconInstagram} alt="instagram-logo" />
        </a>
        <a href="#">
          <img src={iconPinterest} alt="pintereset-logo" />
        </a>
      </div>
    </footer>
  )
}
export default Footer
