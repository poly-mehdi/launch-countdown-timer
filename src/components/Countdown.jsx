const Countdown = ({ days, hours, minutes, seconds }) => {
  return (
    <div className="countdown-container">
      <div className="countdown-item">
        <div className="countdown-tile">
          <h1>{days < 10 ? `0${days}` : days}</h1>
        </div>
        <p>days</p>
      </div>
      <div className="countdown-item">
        <div className="countdown-tile flip-animation">
          <h1>{hours < 10 ? `0${hours}` : hours}</h1>
        </div>
        <p>hours</p>
      </div>
      <div className="countdown-item">
        <div className="countdown-tile flip-animation">
          <h1>{minutes < 10 ? `0${minutes}` : minutes}</h1>
        </div>
        <p>minutes</p>
      </div>
      <div className="countdown-item">
        <div className="countdown-tile flip-animation">
          <h1>{seconds < 10 ? `0${seconds}` : seconds}</h1>
        </div>
        <p>seconds</p>
      </div>
    </div>
  )
}
export default Countdown
