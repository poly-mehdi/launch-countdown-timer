import Countdown from './components/Countdown'
import Footer from './components/Footer'
import Title from './components/Title'
import { getRemainingTime } from './utils/'
import { useEffect, useState } from 'react'

const [daysFirst, hoursFirst, minutesFirst, secondsFirst] = getRemainingTime()

const App = () => {
  const [days, setDays] = useState(daysFirst)
  const [hours, setHours] = useState(hoursFirst)
  const [minutes, setMinutes] = useState(minutesFirst)
  const [seconds, setSeconds] = useState(secondsFirst)
  const [timerFinished, setTimerFinished] = useState(false)

  useEffect(() => {
    const interval = setInterval(() => {
      const [days, hours, minutes, seconds] = getRemainingTime()
      setDays(days)
      setHours(hours)
      setMinutes(minutes)
      setSeconds(seconds)

      if (days === 0 && hours === 0 && minutes === 0 && seconds === 0) {
        clearInterval(interval)
        setTimerFinished(true)
      }
    }, 1000)

    return () => clearInterval(interval)
  }, [])

  return (
    <main>
      <section className="countdown">
        <Title />
        {timerFinished ? (
          <div className="countdown-container">
            <h1 className="time-over">Le compte à rebours est terminé !</h1>
          </div>
        ) : (
          <Countdown
            days={days}
            hours={hours}
            minutes={minutes}
            seconds={seconds}
          />
        )}
        <Footer />
      </section>
    </main>
  )
}

export default App
